# bdr
## birthday reminder

``bdr`` is a simple birthday reminder for unixoide systems written in Haskell.
The user is notified about occurring birthdays since the last check via localhost mail.
It may be used with cornjobs to perform automatic checking.
The birthdays are stored in a plain text file.

### Installation

    $ git clone https://bitbucket.org/andreasdoll/bdr.git
    $ cd bdr/src
    $ ghc Main.hs -o bdr

### Usage

When you run ``bdr`` for the first time, you'll be asked for a file to store the birthdays, and a file to store the date of the last check.

After that you can add birthdays. 
You must provide (in that order) name, day, month (and optionally) year of birth.
You may use only one name, first and lastname as well as multiple names.
Using numbers in the name is fine as long as none of the names contains only numbers.

    $ ./bdr -add Prince 7 5 1958
    $ ./bdr -add Julian Cannonball Adderley 15 9
    $ ./bdr -add Queen Elizabeth the 2nd 21 4 1926 

You can check for birthdays, ideally you'd automate this with a cronjob or similar.
If any birthdays occurred since the last check, you are notified via localhost mail (usually ``/var/spool/mail/you``):

    $ ./bdr -check

You can also list people with unknown year of birth:

    $ ./bdr -missingAge
    Julian Cannonball Adderley:15/09

### License

BSD 2 clause, see ``LICENSE``.

### TODO

- setup cabal project structure
- catch error if no parameters are specified
- catch errors if config file doesn't exist or settings haven't been specified
- catch error if histfile doesn't contain date (first run)
