module Utils
( userName
, homeDir
, dataFile
, getBirthdays
, lastChecked
, appendTo
, parseArguments
, notify
, writeCheck
) where

import Data.Char (isDigit)
import Data.List.Split (splitOn)
import Data.Maybe (fromJust)
import System.Directory (renameFile, removeFile)
import System.Posix.User (getEffectiveUserName, getUserEntryForName, homeDirectory)
import System.Process (runCommand)

import Date (hasNoYear, currentDate, doubleDigit, age, yearOf, ordinalNumber)


userName :: IO String
userName = do
    u <- getEffectiveUserName
    return u


homeDir :: IO String
homeDir = do
    u <- userName
    ue <- getUserEntryForName u
    return $ homeDirectory ue


parseConfig :: IO [(String, String)]
parseConfig = do
    dir <- homeDir
    c <- readFile $ dir ++ "/.config/bdr/config"
    let config = lines c
    return $ asDict config
    where asDict = map tuple . map (splitOn "=")
          tuple x = (head x, last x)


dataFile :: IO String
dataFile = do
    c <- parseConfig
    return $ fromJust(lookup "database" c) -- TODO catch error


getBirthdays :: IO [String]
getBirthdays = do
    f <- dataFile
    bd <- readFile f
    return $ lines bd


histFile :: IO String
histFile = do
    c <- parseConfig
    return $ fromJust(lookup "histFile" c) -- TODO catch error

lastChecked :: IO String
lastChecked = do
    h <- histFile
    lastDate <- readFile h 
    return $ takeWhile (/= '\n') lastDate


writeCheck t = do
    h <- histFile
    let bkp = h ++ ".bkp"
    writeFile bkp t
    removeFile h
    renameFile bkp h


appendTo :: FilePath -> String -> IO ()
appendTo file entry = do 
    appendFile file (entry ++ "\n")


parseArguments :: [String] -> String
parseArguments [] = error "Not enough arguments"
parseArguments args = parseArguments' args [] []


parseArguments' :: [String] -> String -> String -> String
parseArguments' [] name date
    | length name == 0  = error "No name given" 
    | length date < 6   = error "No enough date arguments given" 
    | otherwise         = (init name) ++ ":" ++ (init date)
parseArguments' (a:as) name date 
    | not $ all isDigit a  = parseArguments' as (name ++ a ++ " ") date
    | otherwise            = parseArguments' as name               (date ++ (doubleDigit a) ++ "/")
    

notify :: [String] -> IO ()
notify [] = return ()
notify (bd:bds) = do
    u <- userName
    y <- fmap yearOf currentDate
    let s = subject bd y
    runCommand $ "echo \"\" | mail -s " ++ show s ++ " " ++ u ++"@localhost"
    notify bds
    where subject entry y
            | hasNoYear entry   = nameOf entry ++ " has birthday"
            | otherwise         = nameOf entry ++ " has " ++ (formattedAge entry y) ++ " birthday"
          formattedAge e y = let a = age e y
                             in a ++ ordinalNumber a


nameOf :: String -> String
nameOf s = (splitOn ":" s) !! 0
