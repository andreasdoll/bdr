import System.Environment
import System.Directory (doesFileExist, doesDirectoryExist, createDirectoryIfMissing)
import Data.List

import Utils (getBirthdays, parseArguments, notify, dataFile, appendTo, lastChecked, writeCheck, homeDir)
import Date (hasNoYear, currentDate, withoutYear, nextDay)


main = do
    firstRun
    (command:args) <- getArgs
    let (Just action) = lookup command flags
    action args

flags :: [(String, [String] -> IO ())]
flags =  [ ("-add", addBirthday)
         , ("-check", check)
         , ("-missingAge", missingAge)
         , ("-help", usage)
         , ("-h", usage)
         ]

firstRun = do
    h <- homeDir
    let confDir = h ++ "/.config/bdr"
    let confFile = confDir ++ "/config"
    createDirectoryIfMissing True confDir
    confExists <- doesFileExist confFile
    if not confExists
        then do putStrLn "Setting up configuration:" 
                putStrLn "Where to store birthdays?" 
                db <- getLine
                putStrLn "Where to keep track of the last check date?"
                hf <- getLine
                appendTo confFile $ "database=" ++ db
                appendTo confFile $ "histFile=" ++ hf
                appendTo hf ""
                t <- currentDate
                writeCheck t
        else return ()


addBirthday :: [String] -> IO ()
addBirthday args = do 
    let entry = parseArguments args
    file <- dataFile
    appendTo file entry


check :: [String] -> IO ()
check _ = do
    lc <- lastChecked
    t <- currentDate
    bd <- getBirthdays
    checkSince lc t bd
    writeCheck t
    

checkSince lc t bd
    | lc == t       = return ()
    | otherwise     = checkSince' (nextDay lc) t bd

checkSince' lc t bd
    | lc == t   = notify $ filter (isInfixOf $ withoutYear lc) $ bd
    | otherwise = do 
        notify $ filter (isInfixOf $ withoutYear lc) $ bd
        checkSince' (nextDay lc) t bd


missingAge :: [String] -> IO ()
missingAge _ = do
    bd <- getBirthdays
    let noAge = filter hasNoYear $ bd
    putStr $ unlines noAge


usage ::  [String] -> IO () 
usage _ = putStrLn "bdr [-add name[s] day month [year]] [-check] [-missingAge] [-help]"
