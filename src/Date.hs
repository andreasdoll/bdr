module Date
( currentDate
, yearOf
, withoutYear
, hasNoYear
, age
, doubleDigit
, nextDay
, ordinalNumber
) where

import Data.List (isSuffixOf)
import Data.List.Split (splitOn)
import Data.Time (readTime, UTCTime, addDays, utctDay)
import Data.Time.Clock (getCurrentTime)
import Data.Time.Format (formatTime)
import Data.Time.LocalTime (getCurrentTimeZone, utcToLocalTime, localDay)
import Text.Printf (printf)
import System.Locale (defaultTimeLocale)


currentDate :: IO String
currentDate = do
    now <- getCurrentTime
    timezone <- getCurrentTimeZone
    let zoneNow = utcToLocalTime timezone now
    --let (year, month, day) = toGregorian $ localDay zoneNow
    --return $ doubleDigit (show day) ++ "/" ++ doubleDigit (show month) ++ "/" ++ (show year)
    return $ formatTime defaultTimeLocale "%d/%m/%Y" $ localDay zoneNow


yearOf :: String -> String
yearOf s = reverse $ take 4 $ reverse s


withoutYear :: String -> String
withoutYear = take 5


hasNoYear :: String -> Bool
hasNoYear s = (length $ filter (=='/') s) == 1


age :: String -> String -> String
age s cY = show $ (read cY :: Int) - (read $ yearOf $ dateOf s :: Int)
    where yearOf d = (splitOn "/" d) !! 2
          dateOf d = (splitOn ":" d) !! 1


doubleDigit :: String -> String
doubleDigit n
    | length n == 4     = n
    | length n == 2     = n
    | length n == 1     = printf "%02d" (read n :: Int)
    | otherwise         = error "Invalid date length"

nextDay d = reformat $ addDays 1 $ asDay d
    where asDay d = utctDay (readTime defaultTimeLocale "%d/%m/%Y" d :: UTCTime)
          reformat d = formatTime defaultTimeLocale "%d/%m/%Y" d

ordinalNumber n
    | isSuffixOf "11" ns  = "th"
    | isSuffixOf "12" ns  = "th"
    | isSuffixOf "13" ns  = "th"
    | isSuffixOf "1"  ns  = "st"
    | isSuffixOf "2"  ns  = "nd"
    | isSuffixOf "3"  ns  = "rd"
    | otherwise           = "th"
    where ns = show n

